POSTFIX CONFIG EXAMPLE FOR THE "PARENT" SERVER
===

## HOW-TO

* you might use this examples  on the mail system that sends mails to the smarthost
* to make your postfix use this files , you need `postfix-pcre` package installed
* the assumption here is that the parent host is running `postfix` with the config folder `/etc/postfix`
* also the examples use the fixed ip from the default docker-compose file , so adjust them 
* since the relay host has no sasl authentication , you need a "secure"/"internal" connection to port 25 of the relay, either via docker fixed ip OR vpn ( or something else)
* the relevant postfix settings are:

   ```
   sender_dependent_relayhost_maps = regexp:/etc/postfix/relay_by_sender
   transport_maps = regexp:/etc/postfix/relay_by_recipient
   ```

   you might set them during runtime with:

   ```
   postconf -e "sender_dependent_relayhost_maps = regexp:/etc/postfix/relay_by_sender"
   postconf -e "transport_maps = regexp:/etc/postfix/relay_by_recipient"
   ```