# Docker Postfix Selective Relay

* address verification for recipients
* selective relaying via regex ( e.g. no postmap lmdb:/path/to/my_db is necessary  )
* aims to be GDPR-compliant ( anonymized user@ part by default , logs in default compose are not longer than 2 mbyte, no logs are in persistent places  )

### Installing

* soft-link or copy the docker-compose.yml.example
* create a config folder, containing at least:
   * `config/sasl_passwd`
   * `config/relay_by_sender_regex`
   * `config/relay_by_recipient_regex`

*  create a .env file with the following params
  | param | default | descrption | required | example |
  | REALDOMAIN | true | |   | optional | mail.your-domain.com  |
  | NO_LOG_ANONYMIZE | false | does not redact sender local part ( e.g. name@ ) | optional | true  |
  | LOG_ANON_STRICT | false | logs only first and last character of user part e.g a***z@domain.lan | optional | false  |
  | USE_QUEUE_CONTENT_FILTER | false | will check for non-existent domains in queue.. read below   | optional | true  |



* if you are using a real mail server relaying to this smarthost, see the README.md in `config.REAL-MAILSERVER.example`
* run `docker-compose up --build -d` 
* remember that `relay_by_sender_regex` is superseded by `relay_by_recipient_regex`

* if you try to send via sendmail from inside the container , you might try USE_QUEUE_CONTENT_FILTER since the additional filter verifies dns hosts even from local sendmail , the problem here is: your "real" mail server relaying through this postfix will not see the real queue state since the filter always accepts it and re-queues ( then sends a bounce  )

---

##### A Project of the foundation ( https://the-foundation.gitlab.io )

<div><img src="https://hcxi2.2ix.ch/github.com/TheFoundation/Hocker/README.md/logo.jpg" width="480" height="270"/></div>
