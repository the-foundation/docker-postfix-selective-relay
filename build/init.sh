#!/bin/bash
echo "MAIL::RELAY: INIT"
mkfifo /var/spool/postfix/public/pickup &
(test -e /etc/aliases || touch /etc/aliases ;postalias /etc/aliases ) &

[[ -z "$MAILDOMAIN" ]]      && MAILDOMAIN="postfix-relay.lan"
[[ -z "$MAILNAME" ]]        && MAILNAME=postfix-relay
[[ -z "$POSTMASTER_MAIL" ]] && POSTMASTER_MAIL=default-mail-not-set@using-fallback-default.slmail.me
[[ -z "$REALDOMAIN" ]]      && REALDOMAIN=$MAILDOMAIN

[[ "$POSTMASTER_MAIL" = "default-mail-not-set@using-fallback-default.slmail.me" ]] && echo "SET POSTMASTER MAIL PLEASE , will exit after 5 sec sleep"
[[ "$POSTMASTER_MAIL" = "default-mail-not-set@using-fallback-default.slmail.me" ]] && sleep 5
[[ "$POSTMASTER_MAIL" = "default-mail-not-set@using-fallback-default.slmail.me" ]] && exit 1

[[ -z "$BOUNCE_ADDRESS" ]]  && BOUNCE_ADDRESS=$POSTMASTER_MAIL
echo "$MAILDOMAIN" > /etc/mailname &


[[ "USE_QUEUE_CONTENT_FILTER" = "true" ]] && sed 's/#-o content_filter=filter:dummy/-o content_filter=filter:dummy/g' -i /etc/postfix/master.cf

test -e /config/sender_canonical_maps || ( echo '/^$/    '"$POSTMASTER_MAIL" > /config/sender_canonical_maps ) &
test -e /config/header_checks || (
   (echo '/From: <>/ REPLACE From: '$POSTMASTER_MAIL
    echo '/From: MAILER-DAEMON (Mail Delivery System)/ REPLACE From: '$POSTMASTER_MAIL )> /config/header_checks
) &

(
postconf -e "compatibility_level=2"
postconf -e "bounce_notice_recipient = $BOUNCE_ADDRESS"
postconf -e "mail_name =  $MAILDOMAIN"
postconf -e "myhostname = $MAILNAME"
postconf -e "maillog_file = /dev/stdout"
postconf -e "maillog_file_prefixes = /var, /dev/stdout"
postconf -e "smtp_sasl_tls_security_options = noanonymous"
postconf -e "smtp_tls_security_level = may"
postconf -e "smtp_tls_note_starttls_offer = yes"
# where to find sasl_passwd
postconf -e "smtp_sasl_password_maps = lmdb:/config/sasl_passwd"
postconf -e "header_checks = regexp:/config/header_checks"
postconf -e "sender_canonical_maps = regexp:/config/sender_canonical_maps"
postconf -e "alias_maps = hash:/etc/aliases";
postconf -e "alias_maps = hash:/etc/aliases";
postconf -e "address_verify_sender = <>"
postconf -e "address_verify_sender = postmaster@$REALDOMAIN"
postconf -e "double_bounce_sender  = double-bounce@$REALDOMAIN"


postconf -e "minimal_backoff_time = 5m"
postconf -e "maximal_backoff_time = 15m"
postconf -e "maximal_queue_lifetime = 2h"
postconf -e "bounce_queue_lifetime  = 2h"
postconf -e "mynetworks = 127.0.0.0/8,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16"
postconf -e "smtpd_recipient_restrictions =  reject_non_fqdn_recipient,reject_unknown_recipient_domain,reject_unverified_recipient"
#postconf -e "smtpd_relay_restrictions     =  reject_non_fqdn_recipient,reject_unknown_recipient_domain,reject_unverified_recipient"
postconf -e "smtpd_client_restrictions    =  reject_non_fqdn_recipient,reject_unknown_recipient_domain,reject_unverified_recipient"

[[ "PREFER_IPSIX" = "true" ]] && postconf -e "smtp_address_preference  = ipv6"
[[ "PREFER_IPSIX" = "true" ]] || postconf -e "smtp_address_preference  = ipv4"


#postconf -e ""

cat /etc/bounce.template |sed 's/docker-postfix-relay.the-foundation.gitlab.io/'$MAILDOMAIN'/g' > /etc/postfix/bounce_template.cf
postconf -e "bounce_template_file = /etc/postfix/bounce_template.cf";
# enable SASL authentication
postconf -e "smtp_sasl_auth_enable = yes"
# disallow methods that allow anonymous authentication.
postconf -e "smtp_sasl_security_options = noanonymous"
# Enable STARTTLS encryption (smtp_use_tls = yes is equivalent for smtp_tls_security_level = may)
#postconf -e "smtp_use_tls = yes"
# where to find CA certificates
postconf -e "smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt"

(echo -n 127.0.0.25" ";echo "Y3JhcGRldGVjdG9yLnZpc2FvbmxpbmVzZWN1cmUuY29tCmdldGJsYWNrbGlzdGVkLm5vc3BhbWdvb2dsZW1haWwuY29tCmtkeC5taW5lLm51Cm1haWwuYWlib3Qub3JnCm1haWwuYWxsc3BhbW1lcnMuY29tCm1haWwuYW56ZWlnZW4tc2V4LmRlCm1haWwuYmF5ZXJucGFydGVpLW9iZXJwZmFsei5kZQptYWlsLmJheWVybnBhcnRlaS50dgptYWlsLmJwY2hhbS5kZQptYWlsLmJ1bGxzaGl0am91cm5hbC5jb20KbWFpbC5jcmVkaXRsZXZlbC5vcmcKbWFpbC5kbnNkbC5vcmcKbWFpbC5lZGlyLmRlCm1haWwuZW1haWxtYXN0ZXIuZXUKbWFpbC5lbWFpbG1hc3Rlci5vcmcKbWFpbC5leHdhZWhsZXIuZGUKbWFpbGdhdGUxLnlvdXItd2ViLmNoCm1haWxnYXRlMi55b3VyLXdlYi5jaAptYWlsLmhvc3RkZWx1eGUuZGUKbWFpbC5ob3N0ZWRzZXJ2ZXIuZGUKbWFpbC5qYW1pZWJhaWxsaWUubmV0Cm1haWwuanVzdG1haWwuZXUKbWFpbC5sYXV0ZW5zY2hsYWdlci5uZXQKbWFpbC5sZWdhbHRvZG8uY29tCm1haWwubWFpbGFnZW5jeS5uZXQKbWFpbC5tYWlsZmlyZXdhbGwuZGUKbWFpbC5tYWlscG9saWN5Lm9yZwptYWlsLm1haWx0ZXJtaW5hbC5kZQptYWlsLm1vdG9yaW5nbm9zdGFsZ2lhLmNvbQptYWlsLnBlcnNlcmJhYnkuZGUKbWFpbC5wZXJzZXJwYXJhZGllcy5kZQptYWlsLnJlbW92ZS10aGlzLmNvbQptYWlsLnNwYW1mcmVpLm9yZwptYWlsLnNwYW1zdWNrcy5kZQptYWlsLnN0YWF0c3JhZXNvbi5kZQptYWlsLnN0YWR0YW10Lm5ldAptYWlsLnN0YWR0Z2VtZWluZGUub3JnCm1haWwuc3VuYmx1ZS5kZQptYWlsLnN3aXNzb3guY29tCm1haWwuc3dpc3NveC5uZXQKbWFpbC5zd2lzc294Lm9yZwptYWlsLnRhbmlzYy5jb20KbWFpbC50cmlvc2V4LmRlCm1haWwudHJ1c3RsZXZlbC5vcmcKbXgudWNlcHJvdGVjdC5uZXQKbmlydmFuYS5hZG1pbnMud3MKc210cC1iYWNrdXAxLnBvcnR1bml0eS5kZQpzbXRwLWJhY2t1cDIucG9ydHVuaXR5LmRlCnNtdHAtaW4ucG9ydHVuaXR5LmRlCnNtdHAuc3dpc3NveC5jaAp1bmltYXRyaXguYWRtaW5zLndzCmFudGlzcGFtLnhjb2RlLnJvCg=="|base64 -d |sed 's/^/ /g'|tr -d '\n') >> /etc/hosts &
) &

test -e /var/spool/postfix || mkdir  /var/spool/postfix 
#chown -R postfix /var/spool/postfix  &
chown root /var/spool/postfix & 
## user supplied template
test -e /config/bounce.cf && postconf -e "bounce_template_file = /config/bounce.cf";

bash /postmap.sh &

test -e /config/relay_by_sender_regex       && postconf -e "sender_dependent_relayhost_maps = regexp:/config/relay_by_sender_regex"    &
test -e /config/relay_by_recipient_regex    && postconf                  -e "transport_maps = regexp:/config/relay_by_recipient_regex" &

wait 

echo "MAIL::RELAY: STARTUP"
while (true);do sleep 86400; bash -c "apt-get update ;apt-get -y install ca-certificates;apt-get clean all" &>/dev/shm/ca-cert.upgrade.log;done &


[[ "$LOG_ANON_STRICT" = "true" ]] && export KEEP_USER_CHARS=1 
## will log like a***z@mydomain.lan

export ANO_LOG_MODE=json
#ano_cmd="/email-anonymizer.sh"
ano_cmd="/email-anonymizer-postfix-bash.sh"
[[ "$NO_LOG_ANONYMIZE" = "true" ]] && ano_cmd="/nofilter.sh"
postfix start-fg 2>&1|bash $ano_cmd


