#!/bin/bash

# Simple shell-based filter. It is meant to be invoked as follows:
#       /path/to/script -f sender recipients...

# Localize these. The -G option does nothing before Postfix 2.3.

INSPECT_DIR=/dev/shm/postfilter
SENDMAIL="/usr/sbin/sendmail -G -i" # NEVER NEVER NEVER use "-t" here.
test -e $INSPECT_DIR||mkdir $INSPECT_DIR
# Exit codes from <sysexits.h>
EX_TEMPFAIL=75
EX_UNAVAILABLE=69
RCPT="${@:3}"
RCPT=${RCPT/-- /}

[[ "$DEBUGME=true" ]] && echo "RCPT: $RCPT" >&2



# Clean up when done or when aborting.
trap "rm -f in.$$" 0 1 2 3 15

# Start processing.
cd $INSPECT_DIR || {
    echo $INSPECT_DIR does not exist; exit $EX_TEMPFAIL; }

cat >in.$$ || { 
    echo Cannot save mail to file; exit $EX_TEMPFAIL; }

# Specify your content filter here.
# filter <in.$$ || {
#   echo Message content rejected; exit $EX_UNAVAILABLE; }
[[ -z "$RCPT" ]]      &&  { reason=empty
   echo Message recipient rejected due to $reason; exit $EX_UNAVAILABLE; }

for user in $RCPT;do 

  echo "$user"|grep -q "@"  ||  { reason=no_at_in_rcpt
   echo Message recipient rejected due to $reason; exit $EX_UNAVAILABLE; }
   
   domain=${user//*@/}
   [[ -z "$domain" ]]      &&  { reason=no_domain_for:$user
   echo Message recipient rejected due to $reason; exit $EX_UNAVAILABLE; }

   hostres=$(host $domain 2>&1)
   
  echo "$hostres"|grep -q "NXDOMAIN"  &&  { reason=NXDOMAIN
   echo Message recipient rejected due to $reason; exit $EX_UNAVAILABLE; }
   
   (host -t mx "$domain" 2>&1 |grep  "has no MX record" -q )  &&  nomx=true
   [[ "$nomx" == "true" ]] && { 
       noipsix=true
       noipfour=true
       (host -t aaaa "$domain" 2>&1 |grep -q "has no AAAA record" ||  echo "HAS_IP4") |grep -q HAS_IP4 && noipsix=false
       (host -t    a "$domain" 2>&1 |grep -q "has no A record"    ||  echo "HAS_IP6") |grep -q HAS_IP6 && noipfour=false
       [[ "$DEBUGME" = "true" ]] && echo "nomx $nomx noipfour $noipfour noipsix $noipsix" 
       ## only fail if there is no mx , no ipv4 and no ipv6 
       ( [[ "$nomx" = "true" ]] && [[ "$noipsix" = "true" ]] && [[ "$noipfour" = "true" ]] ) && { reason="NOT_DELIVERABLE: no ipv4/6 or mx record" 
               echo Message recipient rejected due to $reason; exit $EX_UNAVAILABLE; }
    echo -n ; }
done
$SENDMAIL "$@" <in.$$

exit $?

