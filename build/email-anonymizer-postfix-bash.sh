#!/bin/bash
## tsting with: # cat /var/log/mail.log|grep "@"|bash /tmp/logfilter |grep @|grep -v "*"

## USAGE IN  DOCKER: direct with "ANO_LOG_MODE=json"
## USAGE IN CRONTAB:  # 30 3 * * *      /bin/bash -c "time (cat /var/log/mail.log.1 |bash /etc/postfix_mailanon.sh >/tmp/mail.log.1.anonymized;cat /tmp/mail.log.1.anonymized > /var/log/mail.log.1;rm /tmp/mail.log.1.anonymized) || true "  &>/dev/shm/mail.anon.log
jsonemu() { 
  sed -u 's/^/{"msg": "/g;s/$/"}/g'
# ^^ the sed command emulates bokysan.s postfix python log filter
}

[[ "$ANO_LOG_MODE" = "json" ]] && formatcmd() { jsonemu ; } ;
[[ "$ANO_LOG_MODE" = "json" ]] || formatcmd() { cat     ; } ;
[[ -z "$KEEP_USER_CHARS" ]]    && KEEP_USER_CHARS=2

STRIPLEN=$(($KEEP_USER_CHARS+1))
# striplen of 2 will leave first and last letter of local part untouched 

while read line;do 
noprocess=false
[[ "$line" =~ .*@.* ]]                 || noprocess=true
[[ "$line" =~ .*message-id=.* ]]       && noprocess=true
#[[ "$line" =~ .*"sieve: msgid".* ]]   && noprocess=true
[[ "$line" =~ .*"NOQUEUE: reject".* ]] && noprocess=true


[[ "$noprocess" = "true" ]] && echo "$line" | formatcmd
[[ "$noprocess" = "true" ]] || (
  splitline=$(echo "$line"|sed 's/auth USER input: /auth USER input:\n /g;s/ 4.1.1 </ 4.1.1 \n</g;s/)</)\n</g;s/RCPT: /RCPT:\n /g;s/lda(/lda\n(/g;s/)/)\n/g;s/imap(/imap\n(/g;s/)/)\n/g;s/\r//g;s/ \(\|from=\|to=\|user=\)</\n\1\n</g;s/>/>\n/g;')
  echo "$splitline"|while IFS= read -r tmpline;do 
  (
  MYLEN="$STRIPLEN"
  #echo "SPL" "$tmpline"
    tmpskip=false
    [[ "$tmpline" =~ .*@.* ]]  || tmpskip=true
    [[ "$tmpline" =~ ."postmaster@".* ]] && tmpskip=true
    [[ "$tmpline" =~ ."POSTMASTER@".* ]] && tmpskip=true
    [[ "$tmpline" =~ ."mailer-daemon@".* ]] && tmpskip=true
    [[ "$tmpline" =~ ."MAILER-DAEMON@".* ]] && tmpskip=true
    [[ "$tmpline" =~ ."double-bounce@".* ]] && tmpskip=true
    [[ "$tmpline" =~ ."DOUBLE-BOUNCE@".* ]] && tmpskip=true
    [[ "$tmpline" =~ "(".* ]] && MYLEN=3
    [[ "$tmpskip" = "true" ]] && echo "$tmpline"
    [[ "$tmpskip" = "true" ]] || (
       strlen=$(echo -n "${tmpline/@*/}"|wc -c )
       [[ $strlen -le 5 ]] && [[ $MYLEN -ge 3 ]] && MYLEN=2
       head=$(echo "$tmpline" |head -c $MYLEN )
       mid=$( echo "${tmpline/@*/}" |tail -c $MYLEN )
       tail="${tmpline/*@/}"
       echo "${head}***${mid}@${tail}"
      )

    
    #[[ "$tmpline" =~ .*@.* ]] || echo "$tmpline"
)
done |tr -d '\n';echo
)| formatcmd
# ^^ the sed command emulates bokysan.s postfix python log filter

done
